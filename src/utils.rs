use crate::models::SaferNotif;
use chrono::prelude::*;
use rss::{ChannelBuilder, Item};
use scraper::{Html, Selector};

pub fn generate_channel() -> Result<String, ureq::Error> {
    let safer_url = "https://www.safer-aura.fr/website/notifications_liste_&723.html?TYPEB=1&DEPARTEMENT_REF=38&DATEMIN=&DATEMAX=&SURFACEDOSMIN=&SURFACEDOSMAX=&NUMDOSS=";
    // Fetch data from Safer
    let body: String = ureq::get(safer_url).call()?.into_string()?;
    let body = Html::parse_document(&body);
    let selector = Selector::parse("#texteInfo form").unwrap();
    let mut notifications: Vec<SaferNotif> = Vec::new();
    for form in body.select(&selector) {
        let notif = SaferNotif::new(form);
        notifications.push(notif);
    }
    let mut items: Vec<Item> = Vec::new();
    for notif in notifications {
        let mut item = Item::default();
        item.set_title(notif.summary());
        item.set_link(format!("{}{}", safer_url, notif.dossier));
        item.set_content(notif.to_string());
        let dt = Utc
            .datetime_from_str(&format!("{}:00", &notif.date_pub), "%d/%m/%Y %H:%M:%S")
            .expect("too bad");
        item.set_pub_date(dt.format("%Y-%m-%d").to_string());
        //item.set_guid(Guid::default());
        items.push(item);
    }
    // Build the rss feed
    let channel = ChannelBuilder::default()
        .title("Notification Safer AURA")
        .link("safer_url")
        .description("Résultats de recherche notifications")
        .items(items)
        .build();
    Ok(channel.to_string())
}
