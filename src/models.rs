use scraper::{ElementRef, Selector};

pub struct SaferLot {
    pub section: String,
    pub lieu: String,
    pub plan: String,
    pub nat_cad: String,
    pub sub_div: String,
    pub anc_num: String,
    pub surface: String,
}

impl SaferLot {
    pub fn new(line: ElementRef) -> Self {
        let selector = Selector::parse("td").unwrap();
        let mut cols = line.select(&selector);
        let mut section = String::new();
        let mut lieu = String::new();
        let mut plan = String::new();
        let mut nat_cad = String::new();
        let mut sub_div = String::new();
        let mut anc_num = String::new();
        let mut surface = String::new();
        if let Some(item) = cols.next() {
            section = item.text().collect::<String>().trim().to_string();
        }
        if let Some(item) = cols.next() {
            lieu = item.text().collect::<String>().trim().to_string();
        }
        if let Some(item) = cols.next() {
            plan = item.text().collect::<String>().trim().to_string();
        }
        if let Some(item) = cols.next() {
            nat_cad = item.text().collect::<String>().trim().to_string();
        }
        if let Some(item) = cols.next() {
            sub_div = item.text().collect::<String>().trim().to_string();
        }
        if let Some(item) = cols.next() {
            anc_num = item.text().collect::<String>().trim().to_string();
        }
        if let Some(item) = cols.next() {
            surface = item.text().collect::<String>().trim().to_string();
        }
        Self {
            section,
            lieu,
            plan,
            nat_cad,
            sub_div,
            anc_num,
            surface,
        }
    }
    pub fn to_string(&self) -> String {
        let section = format!("Section : {}\t", self.section);
        let lieu = format!("Lieu dit : {}\t", self.lieu);
        let plan = format!("Plan : {}\t", self.plan);
        let nat_cad = format!("Nat. Cad. : {}\t", self.nat_cad);
        let sub_div = format!("Sub-division : {}\t", self.sub_div);
        let anc_num = format!("Anc. N° : {}\t", self.anc_num);
        let surface = format!("Surface : {}\t", self.surface);
        format!(
            "<p>{} | {} | {} | {} | {} | {} | {}</p>\n",
            section, lieu, plan, nat_cad, sub_div, anc_num, surface
        )
    }
}

#[cfg(test)]
mod test_safer_lot {
    use super::*;
    use scraper::Html;
    use std::fs;
    #[test]
    fn safer_lot_new() {
        let body =
            fs::read_to_string("tests/data/sample.html").expect("Could not open sample file");
        let body = Html::parse_document(&body);
        let selector = Selector::parse("form table tbody tr:not([class])").unwrap();
        match body.select(&selector).next() {
            Some(item) => {
                let lot = SaferLot::new(item);
                assert_eq!("AB", lot.section);
                assert_eq!("LE PETIT PORT", lot.lieu);
                assert_eq!("0083", lot.plan);
                assert_eq!("BT", lot.nat_cad);
                assert_eq!("", lot.anc_num);
                assert_eq!("04\u{a0}a\u{a0}10\u{a0}ca", lot.surface);
            }
            None => panic!("Should not be here"),
        };
    }
    #[test]
    fn safer_lot_to_string() {
        let body =
            fs::read_to_string("tests/data/sample.html").expect("Could not open sample file");
        let body = Html::parse_document(&body);
        let selector = Selector::parse("form table tbody tr:not([class])").unwrap();
        match body.select(&selector).next() {
            Some(item) => {
                let lot = SaferLot::new(item);
                let string_to_match = "<p>Section : AB\t | Lieu dit : LE PETIT PORT\t | Plan : 0083\t | Nat. Cad. : BT\t | Sub-division : \t | Anc. N° : \t | Surface : 04\u{a0}a\u{a0}10\u{a0}ca\t</p>\n".to_string();
                assert_eq!(string_to_match, lot.to_string());
            }
            None => panic!("Should not be here"),
        };
    }
}

pub struct SaferNotif {
    /*departement: String,
    commune: String,*/
    pub dossier: String,
    pub batiment: String,
    pub situation: String,
    pub date_rec: String,
    pub date_pub: String,
    pub lots: Vec<SaferLot>,
}

impl SaferNotif {
    pub fn new(form: ElementRef) -> Self {
        // struct field
        let mut dossier = String::new();
        let mut batiment = String::new();
        let mut situation = String::new();
        let mut date_rec = String::new();
        let mut date_pub = String::new();
        // selectors
        let mut lots: Vec<SaferLot> = Vec::new();
        let selector = Selector::parse("p").unwrap();
        let mut par_iter = form.select(&selector);
        if let Some(item) = par_iter.next() {
            let mut ti_iter = item.text();
            ti_iter.next();
            // Numero de dossier
            if let Some(text) = ti_iter.next() {
                dossier = text.trim().to_string();
            }
            ti_iter.next();
            // Bâtiment
            if let Some(text) = ti_iter.next() {
                batiment = text.trim().to_string();
            }
            ti_iter.next();
            ti_iter.next();
            // Situation
            if let Some(text) = ti_iter.next() {
                situation = text.trim().to_string();
            }
        }
        par_iter.next();
        if let Some(item) = par_iter.next() {
            let mut ti_iter = item.text();
            if let Some(text) = ti_iter.next() {
                date_rec = text.trim().to_string();
                let offset = date_rec.find(':').unwrap_or(date_rec.len()) + 1;
                date_rec.drain(..offset);
                date_rec = date_rec.trim().to_string();
            }
            // Date de mise en ligne
            if let Some(text) = ti_iter.next() {
                date_pub = text.trim().to_string();
                let offset = date_pub.find(':').unwrap_or(date_pub.len()) + 1;
                date_pub.drain(..offset);
                date_pub = date_pub.trim().to_string();
            }
        }
        let selector = Selector::parse("table tbody tr:not([class])").unwrap();
        for line in form.select(&selector) {
            lots.push(SaferLot::new(line));
        }
        Self {
            dossier,
            batiment,
            situation,
            date_rec,
            date_pub,
            lots,
        }
    }
    pub fn summary(&self) -> String {
        let dossier = format!("Dossier : {}", self.dossier);
        let batiment = format!("Bâtiment : {}", self.batiment);
        let situation = format!("Situation : {}", self.situation);
        let date_pub = format!("Date de publication : {}", self.date_pub);
        let nbr_lots = format!("Nombre de lots : {}", self.lots.len());
        format!(
            "{} | {} | {} | {} | {}",
            dossier, batiment, situation, date_pub, nbr_lots
        )
    }
    pub fn to_string(&self) -> String {
        let mut output = format!("<p>{}<p><p>Lots</p>", self.summary());
        for lot in &self.lots {
            output = format!("{}\n{}", output, lot.to_string());
        }
        output
    }
}
