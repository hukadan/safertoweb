extern crate chrono;
extern crate hyper;
extern crate rss;
extern crate scraper;
extern crate tokio;
extern crate ureq;
mod models;
mod utils;

use crate::utils::generate_channel;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use std::convert::Infallible;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    // A `Service` is needed for every connection, so this
    // creates one from our `hello_world` function.
    let make_svc = make_service_fn(|_conn| async {
        // service_fn converts our function into a `Service`
        Ok::<_, Infallible>(service_fn(get_feed))
    });

    let server = Server::bind(&addr).serve(make_svc);

    // Run this server for... forever!
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

async fn get_feed(_req: Request<Body>) -> Result<Response<Body>, Infallible> {
    let body = generate_channel();
    let body = match body {
        Ok(text) => text,
        Err(_) => "Error".to_string(),
    };
    Ok(Response::new(body.into()))
}
